const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/eventsDB');
const db = mongoose.connection;

db.on('error', () => {
  console.log('Error db!');
});
db.once('open', () => {
  console.log('Ok!');
});

const Schema = mongoose.Schema;

// Schemas
const Event = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  start_time: {
    type: Number,
    required: true
  },
  end_time: {
    type: Number,
    required: true
  }
});

const EventModel = mongoose.model('Event', Event);

module.exports.EventModel = EventModel;
