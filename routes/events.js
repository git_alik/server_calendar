const express = require('express');

const router = express.Router();

const EventModel = require('../libs/mongoose').EventModel;

router.get('/', (req, res) => {
  EventModel.find({ $and: [{ start_time: { $gte: req.query.from } }, { start_time: { $lt: req.query.to } }] },
  (err, events) => {
    if (!err) {
      return res.send(events);
    }
    res.statusCode = 500;
    return res.send({
      error: 'Server error'
    });
  });
});

router.post('/', (req, res) => {
  const event = new EventModel({
    title: req.body.title,
    description: req.body.description,
    start_time: req.body.start,
    end_time: req.body.end
  });
  event.save((err) => {
    if (!err) {
      return res.send({
        status: 'OK',
        event
      });
    }
    return err;
  });
});

router.put('/', (req, res) => {
  EventModel.findOne({ _id: req.body._id },
  (err, event) => {
    if (err) {
      return err;
    }

    event.title = req.body.title;
    event.description = req.body.description;
    event.start_time = req.body.start;
    event.end_time = req.body.end;

    event.save((error) => {
      if (!error) {
        return res.send({
          status: 'OK',
          event
        });
      }
      return error;
    });
  });
});

router.delete('/:id', (req, res) => {
  EventModel.findOne({ _id: req.params.id },
  (err, event) => {
    event.remove();
    return res.send({
      status: 'OK',
      event
    });
  }
  );
});

module.exports = router;
