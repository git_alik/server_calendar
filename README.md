Сервер для web-приложения "Календарь" - https://github.com/AlikRakhmonov/CalendarApp

Используемые технологии: JavaScript, Node.js, Express, MongoDB, Mongoose 

Инсталляция проекта:

(запускать в ветке - master)

1. Склонить (или скачать архив) проект.
2. Запустить в терминале npm install в папке пректа (необходимо иметь на компьютере установленный NodeJS).
3. Запустить MongoDB (необходимо иметь на компьютере установленную MongoDB).
4. Запустить сервер с базой данных (nodemon или npm start).